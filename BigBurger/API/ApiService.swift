//
//  ApiService.swift
//  BigBurger
//
//  Created by Regis Alla on 07/09/2022.
//

import Foundation
import Alamofire

import Foundation
import Alamofire

class ApiService: NSObject {
    private let url = "https://uad.io/bigburger/"
    static let shared = ApiService()
    var cache: NSCache<NSString, UIImage> = NSCache()
    
    let headers = ["Content-Type": "application/json"]
    var manger: SessionManager!
    private let AUTENTICATE_STATUS_INTERVAL = 200..<499
    
    private override init(){
        self.manger = SessionManager.getManager()
    }
    
    /// Function that allows you to retrieve the list of burgers
    ///
    /// - returns: [Burger] representation of the burger list
    func fetchData(completion : @escaping (BB_Error_TYPE? ,[Burger]?)->()) {
        self.manger.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : self.headers)
            .validate(statusCode: self.AUTENTICATE_STATUS_INTERVAL)
            .responseJSON { response in
                switch response.result {
                case .success:
                    do {
                        let responseContent = try JSONDecoder().decode(BurgerListResponse.self, from: response.data!)
                        guard let burgerList = responseContent.burgers else {
                            return
                        }
                        completion(nil, burgerList)
                    } catch {
                        completion(BB_Error_TYPE.parsingError, nil)
                    }
                case .failure(let error):
                    completion(ErrorHandler.getErrorType(forServerError: error), nil)
                }
            }
    }
    
    /// Function that allows to download an image from a path
    ///
    /// - parameter imagePath: path of image
    /// - returns: UIImage representation of image
    func downloadImageWithPath(imagePath: String, completionHandler: @escaping (UIImage) -> ()) {
        let placeholder = UIImage(named: "ic_no_image")
        
        if let image = self.cache.object(forKey: imagePath as NSString) {
            DispatchQueue.main.async {
                completionHandler(image)
            }
        } else {
            DispatchQueue.main.async {
                completionHandler(placeholder!)
            }
            
            self.manger.request(imagePath, method: .get, parameters: nil, encoding: URLEncoding(destination: .queryString),headers: self.headers)
                .validate(statusCode: self.AUTENTICATE_STATUS_INTERVAL)
                .responseData { response in
                    switch response.result {
                    case .success:
                        DispatchQueue.main.async {
                            if let image = UIImage(data: response.data!) {
                                completionHandler(image)
                            }
                        }
                    case .failure(_):
                        completionHandler(placeholder!)
                    }
                }
        }
    }
}

