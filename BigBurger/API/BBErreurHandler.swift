//
//  BBErreurHandler.swift
//  BigBurger
//
//  Created by Regis Alla on 07/09/2022.
//

import Foundation
import Alamofire

enum BB_Error_TYPE : Error {
    
    //Generic ERRORS
    case networkError
    case serverNotResponding
    case errorServer
    case parsingError
    case notfoundServerError
    case unknownError
}

let SERVER_NOT_FOUND = 502
let SERVER_NOT_RESPONDING = 504
let ERROR_SERVER1 = 500
let ERROR_SERVER2 = 503

struct ErrorHandler {
    
    static func getErrorType(forServerError error : Error)->BB_Error_TYPE {
        guard NetworkReachabilityManager()!.isReachable else{
            return BB_Error_TYPE.networkError
        }
        
        if let serverError =  error as? AFError{
            if let  code = serverError.responseCode{
                return getErrorType(fromStatusCode: code)
            }
        }
        return BB_Error_TYPE.serverNotResponding
    }
    
    static func getErrorType(fromStatusCode code : Int)-> BB_Error_TYPE {
        switch code {
        case SERVER_NOT_FOUND:
            return BB_Error_TYPE.notfoundServerError
        case SERVER_NOT_RESPONDING:
            return BB_Error_TYPE.serverNotResponding
        case ERROR_SERVER1:
            return BB_Error_TYPE.errorServer
        case ERROR_SERVER2:
            return BB_Error_TYPE.errorServer
        default:
            return BB_Error_TYPE.unknownError
        }
    }
    
    static func getErrorMessage(error: BB_Error_TYPE)-> String {
        switch error {
        case .notfoundServerError:
            return NSLocalizedString("SERVER_NOT_FOUND", comment: "")
        case .serverNotResponding:
            return NSLocalizedString("SERVER_NOT_RESPONDING", comment: "")
        case .errorServer:
            return NSLocalizedString("ERR0R_SERVER", comment: "")
        case .networkError:
            return NSLocalizedString("ERR0R_NETWORK", comment: "")
        case .parsingError:
            return NSLocalizedString("ERR0R_PARSING", comment: "")
        case .unknownError:
            return NSLocalizedString("ERROR_UNKNOW", comment: "")
        }
    }
}
