//
//  ArticleViewDataModel.swift
//  BigBurger
//
//  Created by Regis Alla on 07/09/2022.
//

import Foundation
import UIKit

protocol ArticleViewDataModelDelegate: class {
    func didReceiveDataUpdate(data: [Burger])
    func didFailDataUpdateWithError(error: String)
}

class ArticleViewDataModel {
    weak var delegate: ArticleViewDataModelDelegate?
    
    func requestData() {
        Spinner.start()
        ApiService.shared.fetchData { errorType, burgerList in
            Spinner.stop()
            guard let delegate = self.delegate else {
                return
            }
            if let burgerList = burgerList {
                delegate.didReceiveDataUpdate(data: burgerList)
            } else {
                delegate.didFailDataUpdateWithError(error: ErrorHandler.getErrorMessage(error: errorType!))
            }
        }
    }
    
    func loadImageWithPath(imageURL: String, completionHandler: @escaping (UIImage) -> ()) {
        ApiService.shared.downloadImageWithPath(imagePath: imageURL) { (image) in
            completionHandler(image)
        }
    }
}
