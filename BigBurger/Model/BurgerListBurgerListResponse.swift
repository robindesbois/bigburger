//
//  BurgerListResponse.swift
//  BigBurger
//
//  Created by Regis Alla on 07/09/2022.
//

import Foundation

struct BurgerListResponse {
    let burgers: [Burger]?
}

extension BurgerListResponse: Decodable {
    init(from decoder: Decoder) throws {
        self.burgers = try decoder.singleValueContainer().decode([Burger].self)
    }
}
