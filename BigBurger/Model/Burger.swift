//
//  Burger.swift
//  BigBurger
//
//  Created by Regis Alla on 07/09/2022.
//

import Foundation

struct Burger: Codable {
    internal var reference: String?
    internal var title: String?
    internal var description: String?
    internal var imageURL: String?
    internal var price: Int?
    
    private enum CodingKeys: String, CodingKey {
        case reference = "ref"
        case title
        case description
        case imageURL = "thumbnail"
        case price
    }
}
