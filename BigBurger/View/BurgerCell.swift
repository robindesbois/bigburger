//
//  BurgerCell.swift
//  BigBurger
//
//  Created by Regis Alla on 07/09/2022.
//

import Foundation
import UIKit

class BurgerCell : UITableViewCell {
    private let PADDING: CGFloat = 10
    
    var burger: Burger? {
        didSet {
            if let title = burger?.title, let description = burger?.description, let price = burger?.price {
                self.burgerTitleLabel.text = title
                self.burgerDescriptionLabel.text = description
                self.burgerPriceLabel.text = "\(NSLocalizedString("PRICE", comment: "")) : \(Float(price) / 100)€"
            }
        }
    }
    
    private let burgerTitleLabel : UILabel = {
        let burgerTitleLabel = UILabel()
        burgerTitleLabel.textColor = .black
        burgerTitleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        burgerTitleLabel.textAlignment = .left
        burgerTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return burgerTitleLabel
    }()
    
    private let burgerDescriptionLabel : UILabel = {
        let burgerDescriptionLabel = UILabel()
        burgerDescriptionLabel.textColor = .black
        burgerDescriptionLabel.font = UIFont.systemFont(ofSize: 16)
        burgerDescriptionLabel.textAlignment = .left
        burgerDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        burgerDescriptionLabel.numberOfLines = 0
        return burgerDescriptionLabel
    }()
    
    public let burgerImage : UIImageView = {
        let burgerImage = UIImageView(image: UIImage(named: ""))
        burgerImage.contentMode = .scaleAspectFit
        burgerImage.clipsToBounds = true
        burgerImage.translatesAutoresizingMaskIntoConstraints = false
        
        return burgerImage
    }()
    
    var burgerPriceLabel : UILabel = {
        let burgerPriceLabel = UILabel()
        burgerPriceLabel.font = UIFont.boldSystemFont(ofSize: 16)
        burgerPriceLabel.textAlignment = .left
        burgerPriceLabel.textColor = .black
        burgerPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        return burgerPriceLabel
        
    }()
    
    override var alignmentRectInsets: UIEdgeInsets {
        return UIEdgeInsets(top: PADDING, left: PADDING, bottom: PADDING, right: PADDING)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        addSubview(burgerImage)
        addSubview(burgerTitleLabel)
        addSubview(burgerDescriptionLabel)
        addSubview(burgerPriceLabel)
        
        self.backgroundColor = .systemGray6
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 3
        
        self.setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout() {
        self.burgerImage.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.burgerImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.burgerImage.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.burgerImage.widthAnchor.constraint(equalToConstant: frame.size.width / 2).isActive = true
        
        self.burgerTitleLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.burgerTitleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -PADDING).isActive = true
        
        self.burgerTitleLabel.leadingAnchor.constraint(equalTo: self.burgerImage.trailingAnchor).isActive = true
        
        self.burgerDescriptionLabel.topAnchor.constraint(equalTo: self.burgerTitleLabel.bottomAnchor).isActive = true
        self.burgerDescriptionLabel.leadingAnchor.constraint(equalTo: self.burgerImage.trailingAnchor).isActive = true
        self.burgerDescriptionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -PADDING).isActive = true
        
        self.burgerPriceLabel.topAnchor.constraint(equalTo: self.burgerDescriptionLabel.bottomAnchor, constant: PADDING).isActive = true
        self.burgerPriceLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -PADDING).isActive = true
        self.burgerPriceLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -PADDING).isActive = true
    }
}
