//
//  Spinner.swift
//  BigBurger
//
//  Created by Regis Alla on 07/09/2022.
//

import Foundation
import UIKit

public class Spinner {
    static var spinner: UIActivityIndicatorView?
    static var style: UIActivityIndicatorView.Style = .large
    
    static var backgroundColor = UIColor(white: 0, alpha: 0.6)
    static var tintColor = UIColor.black
    
    static func start() {
        guard let firstScene = UIApplication.shared.connectedScenes.first as? UIWindowScene else {
            return
        }
        
        guard let firstWindow = firstScene.windows.first else {
            return
        }
        
        if spinner == nil {
            let frame = UIScreen.main.bounds
            spinner = UIActivityIndicatorView(frame: frame)
            spinner!.backgroundColor = backgroundColor
            spinner!.style = style
            spinner?.color = tintColor
            firstWindow.addSubview(spinner!)
            spinner!.startAnimating()
        }
    }
    
    static func stop() {
        if spinner != nil {
            spinner!.stopAnimating()
            spinner!.removeFromSuperview()
            spinner = nil
        }
    }
}
