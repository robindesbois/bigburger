//
//  ArticleViewController.swift
//  BigBurger
//
//  Created by Regis Alla on 07/09/2022.
//

import Foundation
import UIKit

class ArticleViewController: UITableViewController {
    let cellId = "cellId"
    
    fileprivate var burgerList: [Burger] = [] {
        didSet {
            self.refreshData()
        }
    }
    
    private let dataSource = ArticleViewDataModel()
    
    var controlRefresh: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("NAVIGATION_BAR_TITLE", comment: "")
        
        self.tableView.register(BurgerCell.self, forCellReuseIdentifier: cellId)
        
        tableView.delegate = self
        tableView.dataSource = self
        self.dataSource.delegate = self
        
        self.controlRefresh = UIRefreshControl()
        self.controlRefresh.attributedTitle = NSAttributedString(string: NSLocalizedString("REFESH_MESSAGE", comment: ""))
        self.controlRefresh.backgroundColor = .clear
        self.controlRefresh.tintColor = .black
        self.controlRefresh.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = controlRefresh
        } else {
            self.tableView.addSubview(controlRefresh)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.dataSource.requestData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! BurgerCell
        let curentBurger = self.burgerList[indexPath.row]
        cell.burger = curentBurger
        if let imageURL = curentBurger.imageURL {
            self.dataSource.loadImageWithPath(imageURL: imageURL) { (image) in
                cell.burgerImage.image = image
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.burgerList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.dataSource.requestData()
    }
    
    func refreshData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension ArticleViewController: ArticleViewDataModelDelegate {
    func didFailDataUpdateWithError(error: String) {
        self.controlRefresh.endRefreshing()
        self.tableView.setMessage(error)
    }
    
    func didReceiveDataUpdate(data: [Burger]) {
        if  data.count == 0 {
            self.tableView.setMessage(NSLocalizedString("EMPTY_LIST", comment: ""))
        } else {
            self.tableView.restore()
        }
        self.controlRefresh.endRefreshing()
        self.burgerList = data
    }
}

